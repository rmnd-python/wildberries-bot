tabulate>=0.9.0
aiohttp>=3.8.5
python-dotenv>=1.0.0
aiogram~=3.1.0
APScheduler>=3.10.4
numpy>=1.26.0
pandas>=2.1.1
openpyxl
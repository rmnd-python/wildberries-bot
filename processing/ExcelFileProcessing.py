import os
from typing import List
import json
from logging import getLogger
import numpy as np
import pandas as pd
from .TextProcessing import safe_split_text

def get_json_configuration():
    with open('processing/excel_processing_data.json', encoding='UTF-8') as f:
        data = json.load(f)
        # packing_options = data['packing_options']
        data = data["packing_options_configuration"]
    return data





def log_error(error_message) -> None:
    from logging import getLogger
    logger = getLogger()
    logger.error(error_message)

def calculate_services(dataframe: pd.DataFrame) -> List[List]:
    items = []
    data = get_json_configuration()
    try:

        for i, row in dataframe.iterrows():
            product_name = row['Наименование']
            try:
                packaging_steps = data[
                    str(row['Вариант упаковки']).lower().strip().replace(" ", "")  # Transform to stable form
                ]
            except KeyError as e:
                m = "ОШИБКА. Некорректный вариант упаковки. "
                packaging_steps = [{
                    "name": m * 3,
                    "price": 999999.0
                }]
                log_error(f"{m}. Error:  {str(e)}")
            for step in packaging_steps:
                step_name = step['name']
                try:
                    amount = row['Количество']
                except KeyError:
                    try:
                        amount = row['количество']
                    except KeyError as e:
                        amount = 99999
                        step_name = f"ОШИБКА: не обнаружен столбец 'Количество'| " * 3
                        log_error(f"{step_name}. Error:  {str(e)}")
                try:
                    amount = int(amount)
                except ValueError as e:
                    amount = 99999
                    step_name = f"ОШИБКА:  Нечисловые данные в 'Количество' | " * 3
                    log_error(f"{step_name}. Error:  {str(e)}")

                step_cost = step["price"] * amount
                label = f"{step_name} {product_name}"
                # label = '\n'.join(safe_split_text(label, length=20))
                act_item = [label, amount, step["price"], step_cost]
                items.append(act_item)
    except Exception as e:
        items = []
        log_error(e)
    return items


def process_excel_file(excel_file_path: os.path) -> pd.DataFrame:
    df = pd.read_excel(excel_file_path)
    df.dropna(axis='rows', inplace=True)
    data: List[List] = calculate_services(df)
    if not data:
        return None
    df = pd.DataFrame(data, columns=['Наименование услуг', 'Количество', 'Цена за ед.изм., руб', 'Стоимость услуг, руб'])
    # df.index.name='№'
    df.insert(1, 'Eд.изм.', np.full(df.shape[0], "шт."))
    return df


def get_excel_file(path: os.path, save_to: os.path, file_id: str) -> os.path:
    result_excel: pd.DataFrame = process_excel_file(excel_file_path=path)
    if result_excel is None:
        return None
    result_path = os.path.join(save_to, f"{file_id}.xlsx")
    result_excel.to_excel(result_path, index=False)
    expand(result_path)
    return result_path


def expand(path):
    import openpyxl

    # Load the existing Excel file
    workbook = openpyxl.load_workbook(path)

    # Get the worksheet (e.g., the first sheet)
    worksheet = workbook.active

    # Iterate through all cells in the first column
    for row in worksheet.iter_rows(min_row=1, max_row=worksheet.max_row, min_col=1, max_col=1):
        for cell in row:
            width = 50  # Set text column width
            # Split text to parts
            text_parts: list = safe_split_text(cell.value, length=width)
            # Write the newly split text back into the same cell on separate lines
            cell.value = '\n'.join(text_parts)

            # Adjust row height and column width to fit the new lines of text
            cell.alignment = openpyxl.styles.Alignment(wrap_text=True)
            worksheet.row_dimensions[cell.row].height = len(text_parts) * 15  # Adjust the 15 as needed
            worksheet.column_dimensions[cell.column_letter].width = width


    # Save the modified Excel file
    workbook.save(path)
import os
import json
from dotenv import load_dotenv
load_dotenv()

TG_MAIN_ADMIN_ID = int(os.getenv("TG_MAIN_ADMIN_ID"))
TG_MANAGER_GROUP_ID = int(os.getenv("TG_MANAGER_GROUP_ID"))

from aiogram.filters import BaseFilter, Filter
from aiogram.types import Message



class IsAdmin(Filter):
    """
    Admin filter
    """
    def __init__(self) -> None:
        self.white_list: set = set()
        self.white_list.update(
            (TG_MAIN_ADMIN_ID, TG_MANAGER_GROUP_ID)
        )

    async def __call__(self, message: Message) -> bool:
        return message.from_user.id in self.white_list or message.chat.id in self.white_list



class ExcelAccess(Filter):
    """
    Access to process Excel files via bot
    """
    def __init__(self) -> None:
        with open('access_config.json', encoding='UTF-8') as f:
            data = json.load(f)
            self.white_list: set = set(data['excel_allowed_users'])

    async def __call__(self, message: Message) -> bool:
        return message.from_user.id in self.white_list or message.chat.id in self.white_list

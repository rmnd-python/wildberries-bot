import os
from dotenv import load_dotenv
load_dotenv()

TG_TOKEN = os.getenv("TG_TOKEN")

import asyncio
import logging
from aiogram import Bot, Dispatcher
from aiogram.dispatcher.event.event import EventObserver

from handlers import basic_messages_router, file_router
# from app.set_menu_commands import set_default_command_menu

from apscheduler.schedulers.asyncio import AsyncIOScheduler

scheduler = AsyncIOScheduler()


from app import check_for_feedback_updates, check_for_question_updates
from constants import UPDATE_FREQUENCY, FILES_PATH

os.makedirs(FILES_PATH, exist_ok=True)

async def main() -> None:

    # To store states, we use Redis (while development, it's off - uncomment for production)

    dp = Dispatcher()

    # When shutdown, we close SQL and Redis connections
    observer = EventObserver()
    #
    dp.include_routers(
        basic_messages_router,
        file_router
    )

    bot = Bot(TG_TOKEN, parse_mode="HTML")


    scheduler.add_job(
        check_for_feedback_updates,
        'interval',
        # day=2,
        **UPDATE_FREQUENCY["feedbacks"],
        kwargs={"bot": bot,}
    )
    scheduler.add_job(
        check_for_question_updates,
        'interval',
        # day=2,
        **UPDATE_FREQUENCY["questions"],
        kwargs={"bot": bot,}
    )
    scheduler.start()

    # And the run events dispatching
    await dp.start_polling(bot, skip_updates=True, )



if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO,
                        filename='bot_logs.txt',
                        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    )

    asyncio.run(main())

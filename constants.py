UPDATE_FREQUENCY = {
    "feedbacks": {
        # "minutes": 2,
        "hours": 3,
        # "seconds": 20,
    },
    "questions": {
        # "seconds": 5,
        # "hours": 3,
        "minutes": 10
    },
}
FILES_PATH = "Documents/"
NOTIFICATIONS_REPORT_TEMPLATE = {
    "feedbacks": """
<b>Новые отзывы:</b>\n
- Новых отзывов <b>без текста</b>: {empty};\n
- Новых отзывов <b>c текстом</b>: {filled};\n
<b>Содержание:</b>
{content}
""",
    "questions": """
<b>Новые вопросы:</b>\n
- <b>Всего вопросов</b>: {total};\n
<b>Содержание:</b>
{content}
"""

}
MAX_STRING_SIZE = 16
MAX_MESSAGE_SIZE = 60
import os
import typing as t
from dotenv import load_dotenv
from tabulate import tabulate
load_dotenv()
TG_MANAGER_GROUP_ID = os.getenv("TG_MANAGER_GROUP_ID")
TG_MAIN_ADMIN_ID = os.getenv("TG_MAIN_ADMIN_ID")

LAST_EVENT_ID = {
    "question": None,
    "feedback": None
}
from aiogram import Bot, exceptions

from .AiohttpAPIClient import has_new_questions_or_feedbacks, get_unanswered_questions, get_unprocessed_feedbacks
from constants import UPDATE_FREQUENCY, NOTIFICATIONS_REPORT_TEMPLATE as report
from constants import MAX_STRING_SIZE, MAX_MESSAGE_SIZE
from processing import safe_split_text


def prepare_feedbacks_for_tabulate(feedbacks: t.List[t.Tuple[int, str]]) -> t.Generator[str, None, None]:
    for rate, feedback in feedbacks:
        feedback: str = safe_split_text(feedback, length = MAX_MESSAGE_SIZE)
        # Crop if text exceeds the max length
        f = feedback[0]
        if len(feedback) > 1:
            f = f + "..."
        f: t.List[str] = safe_split_text(f, length=MAX_STRING_SIZE)  # Then, split the text to pieces
        yield rate,  '\n'.join(f) + "\n" + "--" * 8


def prepare_questions_for_tabulate(questions: t.List[t.Tuple[int, str]]) -> t.Generator[str, None, None]:
    for question in questions:
        question: str = safe_split_text(question, length = MAX_MESSAGE_SIZE)
        # Crop if text exceeds the max length
        q = question[0]
        if len(question) > 1:
            q = q+ "..."
        q: t.List[str] = safe_split_text(q, length=MAX_STRING_SIZE)  # Then, split the text to pieces
        yield '\n'.join(q) + "\n" + "--" * 8


def get_tabulated_data(
        data: t.List[t.Tuple[int, str]],
        headers: t.List[str],
        showindex: bool = False,
) -> str:
    """ Function to `tabulate` the data. """
    data = tabulate(data, showindex=showindex, headers=headers,
                     tablefmt="presto", stralign='left', numalign="left"
                     )
    return data


def is_new_event(event: t.Literal['question', 'feedback'], id: str) -> bool:
    if LAST_EVENT_ID[event] == id:
        return False
    else:
        LAST_EVENT_ID[event] = id
        return True


async def check_for_feedbacks() -> t.Union[str, None]:
    new_feedbacks: t.Dict[str, t.Any] = await get_unprocessed_feedbacks()
    if new_feedbacks['success']:
        last_feedback_id = new_feedbacks['last_feedback_id']
        if not is_new_event("feedback", last_feedback_id):
            return

        total_amount = new_feedbacks['total_unanswered']

        new_feedbacks: t.List[t.Tuple[int, str]] = list(
            prepare_feedbacks_for_tabulate(new_feedbacks["feedbacks_with_text"])
        )
        filled_amount = len(new_feedbacks)

        new_feedbacks: str = get_tabulated_data(
            new_feedbacks[:10],  # For detailed output we trim the data
            headers=["Rating ", "Message"]
        ) if new_feedbacks else "<i>Отсутствуют</i>"

        new_feedbacks = report["feedbacks"].format(
            filled=filled_amount,
            empty=total_amount - filled_amount,
            content=new_feedbacks)
        return new_feedbacks
    return None


async def check_for_questions() -> t.Union[str, None]:
    """"""
    new_questions: t.Dict[str, t.Any] = await get_unanswered_questions()
    if new_questions['success']:
        total_amount = new_questions['total_unanswered']
        last_question_id = new_questions['last_question_id']
        if not is_new_event("question", last_question_id):
            return

        new_questions: t.List[t.Tuple[int, str]] = list(
            prepare_questions_for_tabulate(new_questions["questions"]))

        new_questions: str = get_tabulated_data(
            list(map(lambda x: [x], new_questions[:10])),  # Turn List to 2D array
            headers=['Message'], showindex=True
        )

        new_feedbacks = report["questions"].format(
            total=total_amount,
            content=new_questions)
        return new_feedbacks
    return None


async def check_for_feedback_updates(bot: Bot) -> None:
    updates = await has_new_questions_or_feedbacks()
    if not (updates['success'] and updates['feedbacks']):
        # Either failed or no updates
        return
    new_feedbacks = await check_for_feedbacks()
    if new_feedbacks:
        try:
            await bot.send_message(
                chat_id=TG_MANAGER_GROUP_ID,
                text=new_feedbacks,
                reply_markup=None,
            )
        except exceptions.TelegramBadRequest:
            # Bot is not present in that Chat
            try:
                await bot.send_message(
                    chat_id=TG_MAIN_ADMIN_ID,
                    text="Bot CANNOT WRITE TO SUPPOSED GROUP. Add Bot to the chat!",
                    reply_markup=None,
                )
            except exceptions.TelegramBadRequest:
                return


async def check_for_question_updates(bot: Bot) -> None:
    updates = await has_new_questions_or_feedbacks()

    if not (updates['success']):
        # Failed or
        return await bot.send_message(
            chat_id=TG_MAIN_ADMIN_ID,
            text=f"Bot gets ERRORS while trying to request to WB API: {str(updates)}",
            reply_markup=None,
        )
    if not  updates['questions']:
        return

    new_questions = await check_for_questions()
    if new_questions:
        await bot.send_message(
            chat_id=TG_MANAGER_GROUP_ID,
            text=new_questions,
            reply_markup=None,
        )


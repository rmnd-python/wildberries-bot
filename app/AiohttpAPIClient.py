import os
import typing as t
from time import time
from datetime import datetime, timedelta

import asyncio
from logging import getLogger
from dotenv import load_dotenv
import tabulate
from tabulate import tabulate
import aiohttp

# tabulate.PRESERVE_WHITESPACE = True

load_dotenv()
WB_TOKEN = os.getenv("WB_TOKEN")

URLS = {
    "new_q_and_f": "https://feedbacks-api.wildberries.ru/api/v1/new-feedbacks-questions",
    "get_feedbacks": "https://feedbacks-api.wildberries.ru/api/v1/feedbacks",
    "get_questions": "https://feedbacks-api.wb.ru/api/v1/questions",
}


def get_unix_time():
    # Get current time in Unix time
    current_time = int(time())

    # Get time one day ago in Unix time
    one_day = 24 * 60 * 60  # One day in seconds
    one_day_ago = current_time - 3600 * 24  # Subtract one day

    return one_day_ago, current_time


async def has_new_questions_or_feedbacks(
        url=URLS["new_q_and_f"],
        token=WB_TOKEN
) -> t.Dict[str, bool]:
    """ Function to check whether we have new events.  """
    headers = {
        "Accept": "application/json",
        "Authorization": "{}".format(token)
    }

    async with aiohttp.ClientSession() as session:
        async with session.get(url,
                               headers=headers,
        ) as response:
            answer = await response.json()
            if response.status == 200:
                if not answer['error']:  # Success
                    answer = answer['data']
                    answer = dict(
                        success=True,
                        any=answer['hasNewQuestions'] or answer['hasNewFeedbacks'],
                        questions=answer['hasNewQuestions'],
                        feedbacks=answer['hasNewFeedbacks'],
                    )
                else:  # Fail
                    answer = dict(
                        success=False,
                        detail={
                        "error_code": 500,
                        "message": "Internal Error: " + answer['errorText'],
                    }
                    )
            else:
                try:
                    m = answer['errorText']
                except KeyError:
                    try:
                        m = answer['message']
                    except KeyError:
                        m = 'Unknown error'
                finally:
                    answer = dict(
                        success=False,
                        detail={
                            "error_code": response.status,
                            "message": m
                        }
                    )
            return answer



async def get_unprocessed_feedbacks(
        url=URLS["get_feedbacks"],
        token=WB_TOKEN
) -> t.Dict[str, t.Any]:
    """"""
    headers = {
        "Accept": "application/json",
        "Authorization": "{}".format(token),
    }
    # start_time, end_time = get_unix_time()
    body = dict(
        isAnswered=0,  # Not answered
        take=50,
        skip=0,
        order='dateDesc'
    )

    async with aiohttp.ClientSession() as session:
        async with session.get(url,
                               headers=headers, params=body
        ) as response:
            answer = await response.json()
            if response.status == 200:
                if not answer['error']:  # Success
                    answer = answer['data']
                    try:
                        last_id = answer['feedbacks'][0]['id']
                    except IndexError:
                        # Empty
                        return dict(
                            success=True,
                            total_unanswered=0,
                            feedbacks_with_text=[],
                            last_feedback_id=None
                        )

                    total_unanswered = answer['countUnanswered']
                    feedbacks = [
                        # Extract data and trim excessively long sentence
                        (f['productValuation'], f['text']) for f in answer['feedbacks']
                    ]
                    feedbacks_with_text = list(filter(
                        lambda e: e[1], feedbacks)  # Filter all empty strings
                    )

                    answer = dict(
                        success=True,
                        total_unanswered=total_unanswered,
                        feedbacks_with_text=feedbacks_with_text,
                        last_feedback_id=last_id
                    )
                else:  # Fail
                    answer = dict(
                        success=False,
                        detail={
                        "error_code": 500,
                        "message": "Internal Error: " + answer['errorText'],
                    }
                    )
            else:
                try:
                    m = answer['errorText']
                except KeyError:
                    try:
                        m = answer['message']
                    except KeyError:
                        m = 'Unknown error'
                finally:
                    answer = dict(
                        success=False,
                        detail={
                            "error_code": response.status,
                            "message": m
                        }
                    )
            return answer



async def get_unanswered_questions(
        url=URLS["get_questions"],
        token=WB_TOKEN
) -> t.Dict[str, t.Any]:
    """"""
    headers = {
        "Accept": "application/json",
        "Authorization": "{}".format(token),
    }
    # start_time, end_time = get_unix_time()
    body = dict(
        isAnswered=0,  # Not answered
        take=50,
        skip=0,
        order='dateDesc'
    )

    async with aiohttp.ClientSession() as session:
        async with session.get(url,
                               headers=headers, params=body
        ) as response:
            answer = await response.json()
            if response.status == 200:
                if not answer['error']:  # Success
                    answer = answer['data']

                    try:
                        last_id = answer['questions'][0]['id']
                    except IndexError:
                        # Empty
                        return dict(
                            success=True,
                            total_unanswered=0,
                            questions=[],
                            last_feedback_id=None
                        )


                    total_unanswered = answer['countUnanswered']
                    questions = [q['text'] for q in answer['questions']]
                    answer = dict(
                        success=True,
                        total_unanswered=total_unanswered,
                        questions=questions,
                        last_question_id = last_id
                    )
                else:  # Fail
                    answer = dict(
                        success=False,
                        detail={
                        "error_code": 500,
                        "message": "Internal Error: " + answer['errorText'],
                    }
                    )
            else:
                try:
                    m = answer['errorText']
                except KeyError:
                    try:
                        m = answer['message']
                    except KeyError:
                        m = 'Unknown error'
                finally:
                    answer = dict(
                        success=False,
                        detail={
                            "error_code": response.status,
                            "message": m
                        }
                    )
            return answer



if __name__ == "__main__":
    asyncio.run(has_new_questions_or_feedbacks())
    asyncio.run(get_unprocessed_feedbacks())
    asyncio.run(get_unanswered_questions())



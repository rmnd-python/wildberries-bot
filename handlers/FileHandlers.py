import os.path

from aiogram import Bot, Router, F, exceptions

from aiogram.types import Message, FSInputFile
from aiogram.filters import or_f
from aiogram.utils.chat_action import ChatActionSender
from filters import IsAdmin, ExcelAccess  # Custom filters
from aiogram.fsm.context import FSMContext

from constants import FILES_PATH
from processing import get_excel_file

router = Router()
router.message.filter(ExcelAccess())


@router.message(F.document, F.document.file_name.endswith(".xlsx"))
async def excel_file_handler(message: Message, state: FSMContext, bot: Bot):
    """
    Handler to get and process Excel files.
    """
    async with ChatActionSender.typing(bot=bot, chat_id=message.chat.id):
        answer_message: Message = await message.answer("Файл получен. Начинаю скачивание...")
        file_id = message.document.file_id
        file = await bot.get_file(file_id)
        file_path = file.file_path
        file_destination_path = os.path.join(FILES_PATH, file.file_unique_id + ".xlsx")
        await bot.download_file(file_path, file_destination_path)


        answer_message: Message = await answer_message.edit_text("Файл скачан. Обрабатываю...")
        answer_file: str = get_excel_file(file_destination_path, save_to=FILES_PATH, file_id = file_id)
        if not answer_file:
            try:
                os.remove(file_destination_path)
            except FileNotFoundError:
                pass
            return await answer_message.edit_text("Ошибка! Не получилось обработать файл: некорректное содержимое.")

        answer_message: Message = await answer_message.edit_text("Файл обработан. Отправляю результат...")
        file_name = message.date.strftime("АКТ_от_%Y-%m-%d") + ".xlsx"
        result_doc = FSInputFile(answer_file, filename=file_name)

        answer_message: Message = await answer_message.answer_document(result_doc)
        try:
            os.remove(file_destination_path), os.remove(answer_file)
        except FileNotFoundError:
            pass



@router.message(or_f(F.document, F.photo))
async def excel_file_handler(message: Message, state: FSMContext):
    await message.answer("Данный документ <b>не поддерживается</b>. Поддерживаются <b>только</b> файлы формата <i>.xlsx</i>")


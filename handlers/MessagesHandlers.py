from aiogram import Bot, Router, F, exceptions
from aiogram.types import Message, CallbackQuery, InputMediaPhoto, Document, FSInputFile
from aiogram.fsm.context import FSMContext

router = Router()


@router.message(F.text)
async def test_message_handler(message: Message, state: FSMContext, bot: Bot) -> None:
    """
    :param message:
    :param state:
    :return:
    """
    user_id = message.from_user.id
    await message.answer("Привет. Бот работает, не мешай.", reply_markup=None)



